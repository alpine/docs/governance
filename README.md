# Developer Handbook

This is the Antora component for the Alpine Governance Document.
It contains various pertinent information regarding the Alpine Linux project's organization.

## Deploying

You cannot display this "on its own" - you will need to either see the instructions in the [playbook repostory](https://gitlab.alpinelinux.org/alpine/docs/docs.a.o) or write your own Antora playbook.

## Contributing

If you want to send a patch, and your name is not yet in the authors list, please add it there.
Do also note that by contributing the patch, assuming that you have not done so yet, you are agreeing to license your patch (and all future ones) under the license of the repository.
You will be explicitly asked to confirm that in whatever medium you submit that patch (or you may confirm this in your patch commit message).
